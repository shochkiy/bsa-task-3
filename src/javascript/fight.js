import { showWinnerModal } from "./modals/winner";

export function fight(firstFighter, secondFighter) {

  // use variables so not to modify fighters
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  // fighters hit
  while (firstFighterHealth > 0 && secondFighterHealth > 0) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
  }

  // check result
  if (firstFighterHealth > 0) {
    return firstFighter;
  }
  return secondFighter;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
