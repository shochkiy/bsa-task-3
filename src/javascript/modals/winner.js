import { createElement } from '../helpers/domHelper';
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const title = 'Winner!';
  const bodyElement = createWinnerFighterDetails(fighter);
  showModal({title, bodyElement});
}

// Create elements to show winner fighter name and image.
function createWinnerFighterDetails(fighter) {
  const name = fighter.name;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  nameElement.innerText = `Name: ${name}\n`;

  const imageElement = createElement({tagName: 'img', className: 'fighter-image', attributes: { src: fighter.source }});

  fighterDetails.append(nameElement, imageElement);

  return fighterDetails;
}